package org.crc;

import static org.junit.Assert.assertTrue;

import org.crc.algo.Crc32Algo;
import org.crc.algo.CrcCalculator;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    public static final byte[] TestBytes = new byte[]{49,50,51,52,53,54,55,56,57};
    public static final String TestString1 = "123456789";
    public static final String TestString2 = "helloworld";

    private void crcTest(CrcCalculator crcCalculator,byte[] data)
    {
        long crc = crcCalculator.update(data);
        System.out.println(crcCalculator.crcAlgoParams.name +" crc= "
                + Long.toHexString(crc).toUpperCase());
        System.out.println(crcCalculator.crcAlgoParams.name +" check= "
                + Long.toHexString(crcCalculator.crcAlgoParams.check).toUpperCase());

        if (crc != crcCalculator.crcAlgoParams.check) {
            System.out.println(crcCalculator.crcAlgoParams.name + " - BAD ALGO!!! " + Long.toHexString(crc).toUpperCase());
        } else {
            System.out.println(crcCalculator.crcAlgoParams.name + " OK "+ Long.toHexString(crc).toUpperCase());
        }
    }
    private void crcCalc(CrcCalculator crcCalculator,byte[] data)
    {
        long crc = crcCalculator.update(data);
        System.out.println(crcCalculator.crcAlgoParams.name +" crc= "
                + Long.toHexString(crc).toUpperCase());
    }
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void crc32MpegTest1()
    {
        byte[] data = TestString1.getBytes();
        CrcCalculator crcCalculator = new CrcCalculator(Crc32Algo.CRC32_MPEG2);
        System.out.println(TestString1 + " CRC:");
        crcTest(crcCalculator,data);
    }

    @Test
    public void crc32MpegTest2()
    {
        byte[] data = TestString2.getBytes();
        CrcCalculator crcCalculator = new CrcCalculator(Crc32Algo.CRC32_MPEG2);
        System.out.println(TestString2 + " CRC:");
        crcCalc(crcCalculator,data);
    }
}
