package org.crc.algo;

/**
 * CRC Calculator
 * <p> Cyclic Redundancy Check Calculator </p>
 *
 * @author： huangyuxi
 * @date： 2022/1/5 下午 3:59
 */
public class CrcCalculator {
    public CrcAlgoParams crcAlgoParams;
    private byte HashSize = 8;
    private long _mask = 0xFFFFFFFFFFFFFFFFL;
    private long[] _table = new long[256];

    /**
     * @param params
     */
    public CrcCalculator(CrcAlgoParams params)
    {
        crcAlgoParams = params;

        HashSize = (byte) params.hashSize;
        if (HashSize < 64)
        {
            _mask = (1L << HashSize) - 1;
        }

        CreateTable();
    }

    static long reverseBits(long ul, int valueLength)
    {
        long newValue = 0;

        for (int i = valueLength - 1; i >= 0; i--)
        {
            newValue |= (ul & 1) << i;
            ul >>= 1;
        }

        return newValue;
    }

    public long update(byte[] data, int offset, int length)
    {
        long init = crcAlgoParams.refOut ? reverseBits(crcAlgoParams.init, HashSize) : crcAlgoParams.init;
        long hash = ComputeCrc(init, data, offset, length);
        return (hash ^ crcAlgoParams.xorOut) & _mask;
    }

    public long update(byte[] data)
    {
        long init = crcAlgoParams.refOut ? reverseBits(crcAlgoParams.init, HashSize) : crcAlgoParams.init;
        long hash = ComputeCrc(init, data, 0, data.length);
        return (hash ^ crcAlgoParams.xorOut) & _mask;
    }

    private long ComputeCrc(long init, byte[] data, int offset, int length)
    {
        long crc = init;

        if (crcAlgoParams.refOut)
        {
            for (int i = offset; i < offset + length; i++)
            {
                crc = (_table[(int)((crc ^ data[i]) & 0xFF)] ^ (crc >>> 8));
                crc &= _mask;
            }
        }
        else
        {
            int toRight = (HashSize - 8);
            toRight = toRight < 0 ? 0 : toRight;
            for (int i = offset; i < offset + length; i++)
            {
                crc = (_table[(int)(((crc >> toRight) ^ data[i]) & 0xFF)] ^ (crc << 8));
                crc &= _mask;
            }
        }

        return crc;
    }

    private void CreateTable()
    {
        for (int i = 0; i < _table.length; i++)
            _table[i] = CreateTableEntry(i);
    }

    private long CreateTableEntry(int index)
    {
        long r = (long)index;

        if (crcAlgoParams.refIn)
            r = reverseBits(r, HashSize);
        else if (HashSize > 8)
            r <<= (HashSize - 8);

        long lastBit = (1L << (HashSize - 1));

        for (int i = 0; i < 8; i++)
        {
            if ((r & lastBit) != 0)
                r = ((r << 1) ^ crcAlgoParams.poly);
            else
                r <<= 1;
        }

        if (crcAlgoParams.refOut)
            r = reverseBits(r, HashSize);

        return r & _mask;
    }
}
