package org.crc.algo;

/**
 * CRC16
 * <p> CRC16 Algorithm </p>
 *
 * @author： huangyuxi
 * @reference: https://github.com/meetanthony/crcjava
 * @date： 2022/1/5 下午 4:09
 */
public final class Crc16Algo {
    public static CrcAlgoParams CRC16_CCITT_FALSE = new CrcAlgoParams("CRC-16/CCITT-FALSE", 16, 0x1021, 0xFFFF, false, false, 0x0, 0x29B1);
    public static CrcAlgoParams CRC16_ARC = new CrcAlgoParams("CRC-16/ARC", 16, 0x8005, 0x0, true, true, 0x0, 0xBB3D);
    public static CrcAlgoParams CRC16_AUG_CCITT = new CrcAlgoParams("CRC-16/AUG-CCITT", 16, 0x1021, 0x1D0F, false, false, 0x0, 0xE5CC);
    public static CrcAlgoParams CRC16_BUYPASS = new CrcAlgoParams("CRC-16/BUYPASS", 16, 0x8005, 0x0, false, false, 0x0, 0xFEE8);
    public static CrcAlgoParams CRC16_CDMA2000 = new CrcAlgoParams("CRC-16/CDMA2000", 16, 0xC867, 0xFFFF, false, false, 0x0, 0x4C06);
    public static CrcAlgoParams CRC16_DDS110 = new CrcAlgoParams("CRC-16/DDS-110", 16, 0x8005, 0x800D, false, false, 0x0, 0x9ECF);
    public static CrcAlgoParams CRC16_DECTR = new CrcAlgoParams("CRC-16/DECT-R", 16, 0x589, 0x0, false, false, 0x1, 0x7E);
    public static CrcAlgoParams CRC16_DECTX = new CrcAlgoParams("CRC-16/DECT-X", 16, 0x589, 0x0, false, false, 0x0, 0x7F);
    public static CrcAlgoParams CRC16_DNP = new CrcAlgoParams("CRC-16/DNP", 16, 0x3D65, 0x0, true, true, 0xFFFF, 0xEA82);
    public static CrcAlgoParams CRC16_EN13757 = new CrcAlgoParams("CRC-16/EN-13757", 16, 0x3D65, 0x0, false, false, 0xFFFF, 0xC2B7);
    public static CrcAlgoParams CRC16_GENIBUS = new CrcAlgoParams("CRC-16/GENIBUS", 16, 0x1021, 0xFFFF, false, false, 0xFFFF, 0xD64E);
    public static CrcAlgoParams CRC16_MAXIM = new CrcAlgoParams("CRC-16/MAXIM", 16, 0x8005, 0x0, true, true, 0xFFFF, 0x44C2);
    public static CrcAlgoParams CRC16_MCRF4XX = new CrcAlgoParams("CRC-16/MCRF4XX", 16, 0x1021, 0xFFFF, true, true, 0x0, 0x6F91);
    public static CrcAlgoParams CRC16_RIELLO = new CrcAlgoParams("CRC-16/RIELLO", 16, 0x1021, 0xB2AA, true, true, 0x0, 0x63D0);
    public static CrcAlgoParams CRC16_T10_DIF = new CrcAlgoParams("CRC-16/T10-DIF", 16, 0x8BB7, 0x0, false, false, 0x0, 0xD0DB);
    public static CrcAlgoParams CRC16_TELEDISK = new CrcAlgoParams("CRC-16/TELEDISK", 16, 0xA097, 0x0, false, false, 0x0, 0xFB3);
    public static CrcAlgoParams CRC16_TMS37157 = new CrcAlgoParams("CRC-16/TMS37157", 16, 0x1021, 0x89EC, true, true, 0x0, 0x26B1);
    public static CrcAlgoParams CRC16_USB = new CrcAlgoParams("CRC-16/USB", 16, 0x8005, 0xFFFF, true, true, 0xFFFF, 0xB4C8);
    public static CrcAlgoParams CRC16_A = new CrcAlgoParams("CRC-A", 16, 0x1021, 0xc6c6, true, true, 0x0, 0xBF05);
    public static CrcAlgoParams CRC16_KERMIT= new CrcAlgoParams("CRC-16/KERMIT", 16, 0x1021, 0x0, true, true, 0x0, 0x2189);
    public static CrcAlgoParams CRC16_MODBUS = new CrcAlgoParams("CRC-16/MODBUS", 16, 0x8005, 0xFFFF, true, true, 0x0, 0x4B37);
    public static CrcAlgoParams CRC16_X25 = new CrcAlgoParams("CRC-16/X-25", 16, 0x1021, 0xFFFF, true, true, 0xFFFF, 0x906E);
    public static CrcAlgoParams CRC16_XMODEM = new CrcAlgoParams("CRC-16/XMODEM", 16, 0x1021, 0x0, false, false, 0x0, 0x31C3);
}
