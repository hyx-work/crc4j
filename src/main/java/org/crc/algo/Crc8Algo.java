package org.crc.algo;

/**
 * CRC8
 * <p> CRC8 Algorithm </p>
 *
 * @author： huangyuxi
 * @reference: https://github.com/meetanthony/crcjava
 * @date： 2022/1/5 下午 4:09
 */
public final class Crc8Algo {
    public static CrcAlgoParams CRC8 = new CrcAlgoParams("CRC-8", 8, 0x7, 0x0, false, false, 0x0, 0xF4);
    public static CrcAlgoParams CRC8_CDMA2000 = new CrcAlgoParams("CRC-8/CDMA2000", 8, 0x9B, 0xFF, false, false, 0x0, 0xDA);
    public static CrcAlgoParams CRC8_DARC = new CrcAlgoParams("CRC-8/DARC", 8, 0x39, 0x0, true, true, 0x0, 0x15);
    public static CrcAlgoParams CRC8_DVBS2 = new CrcAlgoParams("CRC-8/DVB-S2", 8, 0xD5, 0x0, false, false, 0x0, 0xBC);
    public static CrcAlgoParams CRC8_EBU = new CrcAlgoParams("CRC-8/EBU", 8, 0x1D, 0xFF, true, true, 0x0, 0x97);
    public static CrcAlgoParams CRC8_ICODE = new CrcAlgoParams("CRC-8/I-CODE", 8, 0x1D, 0xFD, false, false, 0x0, 0x7E);
    public static CrcAlgoParams CRC8_ITU = new CrcAlgoParams("CRC-8/ITU", 8, 0x7, 0x0, false, false, 0x55, 0xA1);
    public static CrcAlgoParams CRC8_MAXIM = new CrcAlgoParams("CRC-8/MAXIM", 8, 0x31, 0x0, true, true, 0x0, 0xA1);
    public static CrcAlgoParams CRC8_ROHC = new CrcAlgoParams("CRC-8/ROHC", 8, 0x7, 0xFF, true, true, 0x0, 0xD0);
    public static CrcAlgoParams CRC8_WCDMA = new CrcAlgoParams("CRC-8/WCDMA", 8, 0x9B, 0x0, true, true, 0x0, 0x25);
}
