package org.crc.algo;

/**
 * CRC64
 * <p> CRC64 Algorithm </p>
 *
 * @author： huangyuxi
 * @reference: https://github.com/meetanthony/crcjava
 * @date： 2022/1/5 下午 4:09
 */
public final class Crc64Algo {
    public static CrcAlgoParams CRC64 = new CrcAlgoParams("CRC-64",64, 0x42F0E1EBA9EA3693L, 0x00000000L, false, false, 0x00000000L, 0x6C40DF5F0B497347L);
    public static CrcAlgoParams CRC64_WE = new CrcAlgoParams("CRC-64/WE", 64, 0x42F0E1EBA9EA3693L, 0xFFFFFFFFFFFFFFFFL, false, false, 0xFFFFFFFFFFFFFFFFL,0x62EC59E3F1A4F00AL);
    public static CrcAlgoParams CRC64_XZ = new CrcAlgoParams("CRC-64/XZ", 64, 0x42F0E1EBA9EA3693L, 0xFFFFFFFFFFFFFFFFL, true, true, 0xFFFFFFFFFFFFFFFFL,0x995DC9BBDF1939FAL);
}
