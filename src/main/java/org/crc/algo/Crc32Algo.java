package org.crc.algo;

/**
 * CRC32
 * <p> CRC32 Algorithm </p>
 *
 * @author： huangyuxi
 * @reference: https://github.com/meetanthony/crcjava
 * @date： 2022/1/5 下午 4:09
 */
public final class Crc32Algo {
    public static CrcAlgoParams CRC32 = new CrcAlgoParams("CRC-32", 32, 0x04C11DB7L, 0xFFFFFFFFL, true, true, 0xFFFFFFFFL, 0xCBF43926L);
    public static CrcAlgoParams CRC32_BZIP2 = new CrcAlgoParams("CRC-32/BZIP2", 32, 0x04C11DB7L, 0xFFFFFFFFL, false, false, 0xFFFFFFFFL, 0xFC891918L);
    public static CrcAlgoParams CRC32_C = new CrcAlgoParams("CRC-32C", 32, 0x1EDC6F41L, 0xFFFFFFFFL, true, true, 0xFFFFFFFFL, 0xE3069283L);
    public static CrcAlgoParams CRC32_D = new CrcAlgoParams("CRC-32D", 32, 0xA833982BL, 0xFFFFFFFFL, true, true, 0xFFFFFFFFL, 0x87315576L);
    public static CrcAlgoParams CRC32_JAMCRC = new CrcAlgoParams("CRC-32/JAMCRC", 32, 0x04C11DB7L, 0xFFFFFFFFL, true, true, 0x00000000L, 0x340BC6D9L);
    public static CrcAlgoParams CRC32_MPEG2 = new CrcAlgoParams("CRC-32/MPEG-2", 32, 0x04C11DB7L, 0xFFFFFFFFL, false, false, 0x00000000L, 0x0376E6E7L);
    public static CrcAlgoParams CRC32_POSIX = new CrcAlgoParams("CRC-32/POSIX", 32, 0x04C11DB7L, 0x00000000L, false, false, 0xFFFFFFFFL, 0x765E7680L);
    public static CrcAlgoParams CRC32_Q = new CrcAlgoParams("CRC-32Q", 32, 0x814141ABL, 0x00000000L, false, false, 0x00000000L, 0x3010BF7FL);
    public static CrcAlgoParams CRC32_XFER = new CrcAlgoParams("CRC-32/XFER", 32, 0x000000AFL, 0x00000000L, false, false, 0x00000000L, 0xBD0BE338L);
}
